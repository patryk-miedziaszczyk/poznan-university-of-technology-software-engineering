﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiblitekaSerwera;

namespace Serwer
{
    class Program
    {
        static void Main(string[] args)
        {
            MultiThreadedEchoServer serwer = new MultiThreadedEchoServer();
            serwer.Main();
        }
    }
}
