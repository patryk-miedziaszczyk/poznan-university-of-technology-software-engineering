using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace BiblitekaSerwera
{
    public class Gracz
    {
        public String imie;
        public String nazwisko;
        public String nick;
        public double czas;
        public int liczba_punktow;
        public int miejsce;

        public void set(String imie, String nazwisko, String nick, float czas, int liczba_punktow, int miejsce)
        {
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.nick = nick;
            this.czas = czas;
            this.liczba_punktow = liczba_punktow;
            this.miejsce = miejsce;
        }
        public void print()
        {
            Console.WriteLine(imie + " " + nazwisko + " " + nick + " " + czas + " " + liczba_punktow + " " + miejsce);
        }

        public String toString()
        {
            return imie + " " + nazwisko + " " + nick + " " + czas + " " + liczba_punktow + " " + miejsce;
        }
    }
    public class Ranking
    {
        private List<String> listaRankingowa = new List<String>();

        public void add(Gracz gracz)
        {
            listaRankingowa.Add(gracz.toString());
        }

        public void remove(Gracz gracz)
        {
            listaRankingowa.Remove(gracz.toString());
        }

        public void print()
        {
                Console.WriteLine(listaRankingowa[0]);
        }
        public void sortuj()
        {

            List<String> nieposortowanaListaRankingowa = listaRankingowa;
            List<String> posortowanaListaRankingowa = new List<String>();
            while (nieposortowanaListaRankingowa.Count != 0)
            {
                int max = 0;
                int liczba_punktow = 0;
                int index = 0;
                for (int i = 0; i < nieposortowanaListaRankingowa.Count; i++)
                {
                    String[] subs = nieposortowanaListaRankingowa[i].Split(' ');
                    Console.WriteLine(subs[7]);
                    liczba_punktow = System.Convert.ToInt32(subs[7]);
                    if (liczba_punktow > max)
                    {
                        max = liczba_punktow;
                        index = i;
                    }
                }
                posortowanaListaRankingowa.Add(nieposortowanaListaRankingowa[index]);
                nieposortowanaListaRankingowa.RemoveAt(index);
            }
            System.IO.File.WriteAllLines(@"C:\Users\Admin\source\repos\MultiThreadedEchoServer\Baza.txt", posortowanaListaRankingowa);
        }

        public void doPliku()
        {
            System.IO.File.WriteAllLines(@"C:\Users\Admin\source\repos\MultiThreadedEchoServer\Baza.txt", listaRankingowa);
        }

    }
    public class Sedzia
    {
        public int Sprawdzanie(int x1, int v1, StreamWriter writer)//funkcja sprawdzająca czy wartość została odgadnięta ostatecznie nieużywana
        {
            DateTimeOffset dto;

            dto = DateTimeOffset.Now;
            if (x1 == v1)
            {
                writer.Write("op#" + "sprawdzanie/" + "od#" + "wartoscodgadnieta" + "/" + "id#" + 0 + "/" + "zc#" + dto.ToUnixTimeSeconds() + "/");
                return 1;
            }
            else
            {
                if (x1 > v1)
                {

                    writer.Write("op#" + "sprawdzanie/" + "od#" + "zaduza" + "/" + "id#" + 0 + "/" + "zc#" + dto.ToUnixTimeSeconds() + "/");
                }
                else if (x1 < v1)
                {
                    writer.Write("op#" + "sprawdzanie/" + "od#" + "zamala" + "/" + "id#" + 0 + "/" + "zc#" + dto.ToUnixTimeSeconds() + "/");
                }
                return 0;
            }
        }
    }

    public class Gra
    {
        public void sesja()
        {
            
        }
    }
    public class Administrator
    {
        private String login = "admin";
        private String haslo = "admin123";

        private bool zalogowany = false;

        public String getLogin()
        {
            return login;
        }
        public String getHaslo()
        {
            return haslo;
        }
        public bool getZalogowany()
        {
            return zalogowany;
        }
        public void setZalogowany(bool zalogowany)
        {
            this.zalogowany = zalogowany;
        }
    }
    public class Ustawienia
    {
        public bool pojedynczy_gracz = false;
        public bool wlaczony_czas = false;
    }
    public class MultiThreadedEchoServer
    {
        Ranking ranking = new Ranking();
        public Administrator admin = new Administrator();
        public Gra gra = new Gra();
        public Sedzia sedzia = new Sedzia();
        public int wp = 0; //zmienna zmieniająca wartość jeżeli któryś z graczy wygrał
        public int obliczona; // obliczona średnia wartość nieparzystych liczb 
        public int x = Losowanie();//zmienna przechowująca wylosowaną liczbę
        public DateTime czas = new DateTime(DateTime.Now.Ticks);
        public List<int> intList = new List<int>();// lista przechowująca przesłane liczby nieparzyste
        int proby = 0;
        public void ProcessClientRequests(object argument)
        {
            Gracz gracz = new Gracz();
            DateTimeOffset dto;
            dto = DateTimeOffset.Now;
            char[] buffer = new char[256];// bufor danych
            string theString = null;
            int bytesRead = 0;
            int id = Losowanie();
            DateTime czas = new DateTime(DateTime.Now.Ticks); // teraźniejszy czas
            TcpClient client = (TcpClient)argument;
            try
            {
                System.IO.StreamWriter writer = new System.IO.StreamWriter(client.GetStream());// utworzenie writer do wysyłania danych
                System.IO.StreamReader reader = new System.IO.StreamReader(client.GetStream());// utworzenie reader do odbierania danych
                string s = String.Empty;

                writer.Write("op#" + "polaczenie/" + "od#polaczono" + "/" + "id#" + id + "/" + "zc#" + dto.ToUnixTimeSeconds() + "/"); // jedne z przesyłanych danych

                writer.Flush();
                while (!s.Equals("Exit")) // główna pętla
                {
                    reader.Read(buffer, 0, 256);//odczytywanie przysłanych danych i zapisywanie w buforze

                    s = new string(buffer);
                    Regex regex_liczbaprob = new Regex("(op#)(liczbaprob)(/od#brak)(/id#)([0-9]{0,5})(/wr#)([0-9]{0,5})");// regex sprawdzający przesyłane dane
                    Regex regex_odgadywanie = new Regex("(odgadywanie)/(od#brak)/(id#)([0-9]{0,5})/(wr#)([1-9][0-9]{0,2})");
                    Regex regex_imie = new Regex("(op#)(imie)(/od#brak)(/id#)([0-9]{0,5})(/wr#)([a-z]{0,20})");
                    Regex regex_nazwisko = new Regex("(op#)(nazwisko)(/od#brak)(/id#)([0-9]{0,5})(/wr#)([a-z]{0,20})");
                    Regex regex_nick = new Regex("(op#)(nick)(/od#brak)(/id#)([0-9]{0,5})(/wr#)([a-z]{0,20})");
                    if (regex_imie.IsMatch(s))
                    {
                        MatchCollection matchCollection = regex_imie.Matches(s);
                        foreach (Match match in matchCollection)
                        {
                            gracz.imie = System.Convert.ToString(match.Groups[7].Value);
                            Console.WriteLine(gracz.imie);
                        }
                        writer.Flush();
                    }
                    if (regex_nazwisko.IsMatch(s))
                    {
                        MatchCollection matchCollection = regex_nazwisko.Matches(s);
                        foreach (Match match in matchCollection)
                        {
                            gracz.nazwisko = System.Convert.ToString(match.Groups[7].Value);
                        }
                        writer.Flush();
                    }
                    if (regex_nick.IsMatch(s))
                    {
                        MatchCollection matchCollection = regex_nick.Matches(s);
                        foreach (Match match in matchCollection)
                        {
                            gracz.nick = System.Convert.ToString(match.Groups[7].Value);
                        }
                        writer.Flush();
                    }
                    if (regex_liczbaprob.IsMatch(s))
                    {
                        MatchCollection matchCollection = regex_liczbaprob.Matches(s);
                        foreach (Match match in matchCollection)
                        {

                            intList.Add(System.Convert.ToInt32(match.Groups[7].Value));// dodanie nieparzystej wartości do tablicy
                            id = System.Convert.ToInt32(match.Groups[5].Value);//pobranie id klienta
                        }
                        Console.WriteLine("Zatwierdź odebranie wartości liczby prób od użytkownika o id " + id);
                        Console.ReadKey();
                        obliczona = Obliczanie_Prob(intList[0], intList[1], writer);// obliczenie średiej wartości przesłanych liczb
                        writer.Write("op#" + "liczbaprob/" + "od#" + obliczona + "/" + "id#" + id + "/" + "zc#" + dto.ToUnixTimeSeconds() + "/");
                        writer.Flush();
                    }
                    if (regex_odgadywanie.IsMatch(s))
                    {
                        proby += 1;
                        if (wp == 2)
                        {
                            writer.Write("op#" + "wynik/" + "od#" + "przegrales" + "/" + "id#" + id + "/" + "zc#" + dto.ToUnixTimeSeconds() + "/");
                        }
                        else
                        {
                            MatchCollection matchCollection = regex_odgadywanie.Matches(s);
                            foreach (Match match in matchCollection)
                            {
                                Console.WriteLine("Podana wartość użytkownika o id " + id + " to: " + match.Groups[6].Value);
                                int odpowiedz = sedzia.Sprawdzanie(System.Convert.ToInt32(match.Groups[6].Value), x, writer);
                                if (odpowiedz == 1)// sprawdzanie czy wartość jest wartością wylosowaną 
                                {
                                    wp = 2;
                                    gracz.liczba_punktow = 1000 - (proby * 10);
                                    gracz.czas = 0;
                                    gracz.miejsce = 1;
                                    break;
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }
                    }
                    writer.Flush();
                }
                reader.Close();
                writer.Close();
                client.Close();
                Console.WriteLine("Kończenie połączenia z klientem!");
            }
            catch (IOException)
            {
                Console.WriteLine("Połączenie zostało zakończone.");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
            ranking.add(gracz);
            ranking.sortuj();
        }

        public void Main()
        {
            Ustawienia ustawienia = new Ustawienia();
            TcpListener listener = null;
            while (admin.getZalogowany() != true)
            {
                Console.WriteLine("Proszę podać login: ");
                String login = Console.ReadLine();
                Console.WriteLine("Proszę podać haslo: ");
                String haslo = Console.ReadLine();
                if (admin.getLogin() == login && admin.getHaslo() == haslo)
                {
                    String odpowiedz = " ";
                    while (odpowiedz != "4")
                    {
                        admin.setZalogowany(true);
                        Console.WriteLine("------MENU------");
                        Console.WriteLine("1 - Zmiana ustawień serwera");
                        Console.WriteLine("2 - Wyświetlenie rankingu");
                        Console.WriteLine("3 - Uruchomienie serwera");
                        Console.WriteLine("4 - Wyłączenie serwera");
                        Console.WriteLine();
                        odpowiedz = Console.ReadLine();
                        if (odpowiedz == "1")
                        {
                            String wybor = " ";
                            while (wybor != "0")
                            {
                                Console.WriteLine("0 - zatwierdzenie ustawień");
                                Console.WriteLine("1 - pojedynczy gracz = " + ustawienia.pojedynczy_gracz);
                                Console.WriteLine("2 - wlaczony czas = " + ustawienia.wlaczony_czas);
                                Console.WriteLine();
                                wybor = Console.ReadLine();
                                if (wybor == "1")
                                {
                                    Console.WriteLine("1 - pojedynczy gracz = włączony");
                                    Console.WriteLine("0 - pojedynczy gracz = wyłączony");
                                    String wybrana_wartosc = Console.ReadLine();
                                    if (wybrana_wartosc == "1")
                                    {
                                        ustawienia.pojedynczy_gracz = true;
                                    }
                                    else
                                    {
                                        ustawienia.pojedynczy_gracz = false;
                                    }
                                }
                                else if (wybor == "2")
                                {
                                    Console.WriteLine("1 - czas = włączony");
                                    Console.WriteLine("0 - czas = wyłączony");
                                    String wybrana_wartosc = Console.ReadLine();
                                    if (wybrana_wartosc == "1")
                                    {
                                        ustawienia.wlaczony_czas = true;
                                    }
                                    else
                                    {
                                        ustawienia.wlaczony_czas = false;
                                    }
                                }
                                else if (wybor == "0")
                                {
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("Błędny numer działania, proszę podać poprawny numer działania");
                                }
                            }

                        }
                        else if (odpowiedz == "2")
                        {
                            Console.WriteLine("------RANKING------");
                            ranking.print();
                        }
                        else if (odpowiedz == "3")
                        {
                            try
                            {
                                Console.WriteLine("Wpisz adres ip:");
                                String ip = null;
                                ip = Console.ReadLine();
                                listener = new TcpListener(IPAddress.Parse(ip), 39999);// ustawienie nasłuchiwania na dany port
                                listener.Start(); // start nasłuchiwania
                                Console.WriteLine("Serwer włączony");
                                TcpClient client1 = listener.AcceptTcpClient();// zaakceptowanie klienta
                                Console.WriteLine("Zaakceptowano nowe połączenie klienta...");
                                Thread t1 = new Thread(ProcessClientRequests);//utworzenie pierwszego wątku
                                t1.Start(client1);//start wątku
                                TcpClient client2 = listener.AcceptTcpClient();// zaakceptowanie klienta
                                Console.WriteLine("Zaakceptowano nowe połączenie klienta...");
                                Thread t2 = new Thread(ProcessClientRequests);//utworzenie drugiego wątku
                                t2.Start(client2);//start wątku
                                Console.WriteLine("Wylosowana liczba z zakresu 0-100 to: " + x);
                                Console.WriteLine("Oczekiwanie na dane od użytkowników... ");
                                odpowiedz = "4";
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                            finally
                            {
                                if (listener != null)
                                {
                                    listener.Stop();//zakończenie nasłuchiwania 
                                }
                            }
                        }
                        else if (odpowiedz == "4")
                        {
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Błąd");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Nieprawidłowe dane logowania");
                }
            }
        }
        static public int Losowanie()// funkcja losująca liczbę do odgadnięcia
        {
            Random rnd = new Random();

            int poczatek = 0;//zakres od
            int koniec = 100;//zakres do
            int wylosowana = rnd.Next(poczatek, koniec);//losowanie
            return wylosowana;//zwrócenie wartosci wylosowanej
        }
        static public int Obliczanie_Prob(int x1, int x2, StreamWriter writer)//funkcja obliczająca liczbę prób ostatecznie nieużywana
        {
            DateTimeOffset dto;

            dto = DateTimeOffset.Now;
            int obliczona;

            obliczona = (x1 + x2) / 2;
            return obliczona;

        }
    }
}